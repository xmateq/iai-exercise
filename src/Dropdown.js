import React, {useState, useRef} from 'react'
import styled from 'styled-components';
import response from './response';
import {IoIosArrowDown} from 'react-icons/io';
import { px2vw } from './globalStyles';

const Container = styled.div`
    position: relative;

    .dropdown {
        background: #fff;
        border-radius: 8px;
        position: absolute;
        width: ${px2vw(180)};
        box-shadow: 0 1px 8px rgba(0, 0, 0, 0.3);
        opacity: 0;
        visibility: hidden;
        transform: translateY(-20px);
        transition: opacity 0.4s ease, transform 0.4s ease, visibility 0.4s;
    }

    .dropdown.active {
        opacity: 1;
        visibility: visible;
        transform: translateY(0);
        width: ${px2vw(320)};
    }

    .dropdown ul {
        list-style: none;
        padding: 0;
        margin: 0;
    }

    .dropdown li {
        padding: 3px;
        cursor: pointer;
    }

    .dropdown li:hover {
        background: #0090F6;
        color: white;
    }
`

const Button = styled.button`
    width: ${px2vw(330)};
    margin-right: ${px2vw(10)};
    min-width: 100px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    color: black !important;
    outline-color: black !important;
    background-color: white;
    border: 1px solid lightgray;
    padding: ${px2vw(8)} ${px2vw(20)};
    border-radius: 5px;
    cursor: pointer;

    :focus {
        outline: none;
    }
`

const ArrowIcon = styled(IoIosArrowDown)`
    font-size: ${px2vw(20)};
`

function Dropdown({ setImage, setLink }) {

    const [active, setActive] = useState(false);
    const [color, setColor] = useState(null);
    const dropdownRef = useRef(null);

    // Changing photos based on color, taken photos from the internet
    const colorChangeToSilver = (e) => {
        setColor(e.target.innerHTML);
        setActive(!active);
        setLink(response.multiversions[0].items['1-1'].products[0].url)
        setImage('https://www.skinit.com/media/catalog/product/cache/9dbe6a0c16a5b581719a1aa389879cfc/p/r/product_s_i_silver-carbon-fiber-xbox-one-s-console-and-controller-bundle-skin-1517596148_skntxtcbfraxbx1sb-pr-01_80.jpg')
    }

    const colorChangeToBlack = (e) => {
        setColor(e.target.innerHTML);
        setActive(!active);
        setLink(response.multiversions[0].items['1-2'].products[0].url)
        setImage('https://www.komputronik.pl/informacje/wp-content/uploads/2017/05/2d0c0efa-7d89-4969-b74d-214126888908.jpg')
    }

    const colorChangeToWhite = (e) => {
        setColor(e.target.innerHTML);
        setActive(!active);
        setLink(response.multiversions[0].items['1-3'].products[0].url)
        setImage('https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RW2OuB?ver=2804')
    }

    return (
        <Container>
            <Button onClick={() => setActive(!active)}>
                <p>{color}</p>
                <ArrowIcon />
            </Button>
            <nav ref={dropdownRef} className={`dropdown ${active ? 'active': 'inactive'}`}>
                <ul>
                    <li onClick={colorChangeToSilver} >{response.multiversions[0].items['1-1'].values[61].name}</li>
                    <li onClick={colorChangeToBlack} >{response.multiversions[0].items['1-2'].values[60].name}</li>
                    <li onClick={colorChangeToWhite} >{response.multiversions[0].items['1-3'].values[59].name}</li>
                </ul>
            </nav>
        </Container>
    )
}

export default Dropdown
