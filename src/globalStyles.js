import { createGlobalStyle } from 'styled-components'

// Convert pixels to vw, source:
// https://dev.to/carloscne/creating-responsive-and-adaptive-layouts-with-react-and-styled-components-1ghi
export const px2vw = (size, width = 1440) => `${(size/ width) * 100}vw`;

export const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;
        font-family: 'Arial', sans-serif;
    }

    :root {
        font-size: ${px2vw(16)};

        @media (min-width: 768px) {
            font-size: ${px2vw(16)};
        }

        @media (min-width: 1024px) {
            font-size: ${px2vw(13)};
        }
    }
`
