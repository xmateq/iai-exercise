import React, { useState } from 'react'
import styled from 'styled-components';
import { MdClose } from 'react-icons/md';
import {FaCheck} from 'react-icons/fa';
import {ImCross} from 'react-icons/im';
import {FcAlarmClock} from 'react-icons/fc';
import {IoIosArrowBack} from 'react-icons/io';
import {IoIosArrowForward} from 'react-icons/io';
import response from './response';
import Dropdown from './Dropdown';
import { px2vw } from './globalStyles';

const Background = styled.div`
    width: 100%;
    height: 100%;
    background: rgba(0, 0, 0, 0.8);
    position: fixed;
    display: flex;
    justify-content: center;
    align-items: center;
`
const PopupWrapper = styled.div`
    width: ${px2vw(740)};
    height: 420px;
    min-width: 300px;
    box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
    background: #fff;
    color: #000;
    display: flex;
    position: relative;
    z-index: 10;
    border-radius: 5px;
    transition: top 0.4s, opacity 0.4s;

    @keyframes popupFade {
        from {transform: translateY(-50%);opacity: 0;}
        to {transform: translateY(0);opacity: 1;}
    }

    animation-name: popupFade;
    animation-duration: 0.5s;
`
const PopupImageContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`
const PopupImg = styled.img`
    height: 150px;
    width: ${px2vw(285)};
    align-self: center;
    object-fit: cover;
    border-radius: 10px 0 0 10px;
    background: #fff;
`
const PopupContent = styled.div`
    display: flex;
    flex-direction: column;
    line-height: 1.8;
    color: #141414;

    #name {
        font-weight: bold;
        font-size: 18px;
        width: ${px2vw(300)};
        position: relative;
        margin-top: 30px;
        line-height: 1.2;
    }

    #price {
        font-weight: bold;
        font-size: 18px;
        color: #0090F6;
        margin: 7px 0;
    }

    #size {
        font-size: 13px;
    }
`
const VariantButtons = styled.div`
    margin-bottom: 15px;
    width: ${px2vw(330)};
    button {
        margin-right: 10px;
        outline-color: #0090F6;
        border: 1px solid gray;
        padding: ${px2vw(10)} ${px2vw(14)};
        background-color: #fff;
        color: gray;
        border-radius: 5px;
        cursor: pointer;
    }

    button:focus {
        color: black;
    }
`
const ShippingDetails = styled.div`
    display: flex;
    margin-top: 15px;
    width: ${px2vw(350)};
`
const Available = styled.div`
    display: flex;
    align-items: center;

    p {
        margin-left: ${px2vw(10)};
        margin-right: ${px2vw(20)};
    }
`
const Unavailable = styled.div`
    display: flex;
    align-items: center;

    p {
        margin-left: ${px2vw(10)};
        margin-right: ${px2vw(20)};
    }
`
const SendPossible = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;

    #sendPossibleText {
        > p {
            font-size: ${px2vw(12)};
            margin-left: ${px2vw(10)};
            line-height: 1.2;
        }

        #blue {
            color: #0090F6;
        }

        #blue:hover {
            text-decoration: underline;
            cursor: pointer;
        }
    }
`
const BuyButtons = styled.div`
    display: flex;
    margin-top: ${px2vw(20)};

    #count {
        padding: ${px2vw(10)} ${px2vw(20)};
        width: ${px2vw(120)};
        height: 5vh;
        margin-right: 10px;
        border: 1px solid lightgray;
        color: gray;
        display: flex;
        align-items: center;
        justify-content: space-between;
        border-radius: 4px;
    }
    form #addToCart {
        padding: ${px2vw(10)} ${px2vw(20)};
        width: ${px2vw(200)};
        height: 5vh;
        border-radius: ${px2vw(5)};
        background-color: #0090F6;
        color: white;
        font-weight: 700;
        border: none;
    }

    #addToCart:focus {
        outline: none;
    }

    #count {
        display: flex;
        justify-content: space-between;
    }

    span:hover {
        cursor: pointer;
    }

    span {
        color: black;
    }

    #addToCart:hover {
        cursor: pointer;
    }

    #addToCart:active {
        opacity: 0.7;
    }
`
const CloseButton = styled(MdClose)`
    cursor: pointer;
    position: absolute;
    top: 20px;
    right: 20px;
    width: 32px;
    height: 32px;
    padding: 0;
    z-index: 10;
`
const CheckIcon = styled(FaCheck)`
    color: #15bd15;
`
const CrossIcon = styled(ImCross)`
    color: red;
    font-size: 12px;
`
const ClockIcon = styled(FcAlarmClock)`

`
const PreviousIcon = styled(IoIosArrowBack)`
    margin-left: 20px;
    cursor: pointer;
`
const NextIcon = styled(IoIosArrowForward)`
    margin-right: 20px;
    cursor: pointer;
`

function Popup({ showPopup, setShowPopup }) {

    const [count, setCount] = useState(0);
    const [link, setLink] = useState(response.product.link);
    const [available, setAvailable] = useState(true);
    const [price, setPrice] = useState(response.sizes.items.U.price.toFixed(2));
    const [image, setImage] = useState('https://www.skinit.com/media/catalog/product/cache/9dbe6a0c16a5b581719a1aa389879cfc/p/r/product_s_i_silver-carbon-fiber-xbox-one-s-console-and-controller-bundle-skin-1517596148_skntxtcbfraxbx1sb-pr-01_80.jpg')

    // Functions for setting variant and price
    const checkUAvailableAndPrice = () => {
        if (response.sizes.items.U.amount > 0) {
            setAvailable(true)
        } else {
            setAvailable(false)
        }

        setPrice(response.sizes.items.U.price.toFixed(2))
    }

    const checkVAvailableAndPrice = () => {
        if (response.sizes.items.V.amount > 0) {
            setAvailable(true)
        } else {
            setAvailable(false)
        }

        setPrice(response.sizes.items.V.price.toFixed(2))
    }

    const checkWAvailableAndPrice = () => {
        if (response.sizes.items.W.amount > 0) {
            setAvailable(true)
        } else {
            setAvailable(false)
        }

        setPrice(response.sizes.items.W.price.toFixed(2))
    }

    return (
        <>
            {showPopup ? (
                <Background>
                    <PopupWrapper showPopup={showPopup}>
                        <PopupImageContainer>
                            <PreviousIcon />
                            <PopupImg src={image}></PopupImg>
                            <NextIcon />
                        </PopupImageContainer>
                        <PopupContent>
                            <p id="name">{response.product.name}</p>
                            <p id="price">{price + " zł"}</p>
                            <p id="size">Rozmiar: </p>
                            <VariantButtons>
                                <button onClick={checkUAvailableAndPrice}>{response.sizes.items.U.name}</button>
                                <button onClick={checkVAvailableAndPrice}>{response.sizes.items.V.name}</button>
                                <button onClick={checkWAvailableAndPrice}>{response.sizes.items.W.name}</button>
                            </VariantButtons>
                            <p>Wariant: </p>
                            <Dropdown setImage={setImage} setLink={setLink} />
                            <ShippingDetails>
                                {available ? (
                                    <Available>
                                        <CheckIcon />
                                        <p>Produkt dostępny</p>
                                    </Available>
                                ): (
                                    <Unavailable>
                                        <CrossIcon />
                                        <p>Produkt niedostępny</p>
                                    </Unavailable>
                                )}
                                <SendPossible>
                                    <div id="clockIcon">
                                        <ClockIcon />
                                    </div>
                                    <div id="sendPossibleText">
                                        <p>Możemy wysłać już dzisiaj!</p>
                                        <p id="blue">Sprawdź czasy i koszty wysyłki</p>
                                    </div>
                                </SendPossible>
                            </ShippingDetails>
                            <BuyButtons>
                                <div id="count">
                                    <span onClick={count === 0 ? () => null : () => setCount(count - 1)}>-</span> <span>{count}</span> <span onClick={() => setCount(count + 1)}>+</span>
                                </div>
                                <form action={link}>
                                    <button type="submit" id="addToCart">
                                        Dodaj do koszyka
                                    </button>
                                </form>
                            </BuyButtons>
                        </PopupContent>
                        <div className="cross">
                            <CloseButton aria-label="Close popup" onClick={() => setShowPopup(prev => !prev)}></CloseButton>
                        </div>
                    </PopupWrapper>
                </Background>
            ): null}   
        </>
    )
}

export default Popup;
