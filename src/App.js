import React, {useState} from 'react';
import styled from 'styled-components';
import { GlobalStyle } from './globalStyles';
import Popup from './Popup';


const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`

const Button = styled.button`
  min-width: 100px;
  padding: 16px 32px;
  border-radius: 4px;
  border: none;
  background: #0090F6;
  color: #fff;
  font-size: 24px;
  cursor: pointer;
`

function App() {

  const [showPopup, setShowPopup] = useState(false);

  const handlePopupOpen = () => {
    setShowPopup(prev => !prev)
  }

  return (
    <>
      <Container>
        <Button onClick={handlePopupOpen}>Click Me !</Button>
        <Popup showPopup={showPopup} setShowPopup={setShowPopup} />
        <GlobalStyle />
      </Container>
    </>
  );
}

export default App;
